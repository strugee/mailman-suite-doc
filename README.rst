=====================================
Documentation for Mailman Suite Setup
=====================================

This repo is home for the documentation about `development setup of Mailman Suite`_.

.. _`development setup of Mailman Suite`: http://docs.mailman3.org/en/latest/devsetup.html
